// Copyright JJsoft.inc 2017

#pragma once

#include "Components/ActorComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	float Reach = 100.f;

	UPROPERTY(VisibleAnyWhere)
	UPhysicsHandleComponent* PhysicsHandle = nullptr;

	UPROPERTY(VisibleAnyWhere)
	UInputComponent* InputComponent = nullptr;


	void Grab();
	void Release();

	void SetupInputComponent();

	void FindPhysicsComponent();

	const FHitResult GetFirstPhysicsBodyInReach();

	const FVector GetReachLineEnd();
	const FVector GetReachLineEnd(FVector &OutStartLine);
};
